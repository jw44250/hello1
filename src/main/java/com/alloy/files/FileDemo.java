package com.alloy.files;

import java.io.File;
import java.io.IOException;

public class FileDemo {

	public static void main(String[] args) {
		
		// File file = new File("hello.txt"); current directory not given full Path
		
		// should be using forward slash 
		
		File file = new File("hello.txt");
		
		try {
			boolean isFileCreated = file.createNewFile();
			System.out.println("File created :" + isFileCreated);		
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}
}
